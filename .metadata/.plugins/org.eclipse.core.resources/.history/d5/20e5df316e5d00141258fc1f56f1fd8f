\documentclass{article}
\input{503macros.tex}

\usepackage{amsfonts, amsmath, amsthm}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}

\def\Sometime{\mathord{\mathsf{F}}}
\def\Forever{\mathord{\mathsf{G}}}
\def\Next{\mathord{\mathsf{O}}}
\def\NextX{\mathord{\mathsf{X}}}
\def\Until{\mathrel{\mathsf{U}}}
\def\Release{\mathrel{\mathsf{R}}}
\def\WeakUntil{\mathrel{\mathsf{W}}}
\def\Before{\mathrel{\mathsf{B}}}
\def\True{\mathord{\mathsf{true}}}
\def\All{\mathord{\mathsf{A}}}
\def\Exists{\mathord{\mathsf{E}}}
\def\Every{\mathord{\mathsf{E}}}

\title{CSC 503 Homework Assignment 8}
\author{Due October 27, 2014}
\date{October 20, 2014}

\begin{document}
\maketitle

\begin{itemize}
\item \textbf{[80 points total]} Consider the transition model ${\cal M}_1$
  depicted in Figure \ref{f1}.
  \begin{figure}[h]
    \centering
    \caption{Model ${\cal M}_1$}
\begin{center}

\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=3cm]
  \node[state] (q3)      {$s_3: \bar a b$};
  \node[state] (q4)    [right of=q3] {$s_4: \bar a \bar b$};
  \node[initial,state] (q1)   [below of=q3]   {$s_1: a b$};
  \node[state] (q2)   [right of=q1]   {$s_2:  a \bar b$};

  \path[->] 
        (q3) edge         node {} (q4)
        (q4) edge [loop right] node {} (q4)
        (q1) edge         node {} (q3)
        (q2) edge         node {} (q4)
        (q2) edge [loop right] node {} (q2)
        (q1) edge    [bend left] node {} (q2)
        (q2) edge    [bend left] node {} (q1);
\end{tikzpicture}
\end{center}
\label{f1}
\end{figure}
  \par
  In answering the following questions, recall that all paths are
  infinite.  To indicate a path that ends with a repeated set of
  states, put parenthesis around the repeated subsequence (e.g., $(q,
  q'', q''')$.  To indicate a path in which an initial subsequence is
  followed by any possible continuing path, write ``(any)'' after
  giving the initial sequence.
  \begin{enumerate}
  \item \textbf{[8 points]} Find a path from the initial state $s_1$
    which satisfies $\Forever a$.
    \begin{answer}
    	\textbf{Path}: $s_1 - (s_2)$ \newline
    	\textit{Moving from $s_1$ to $s_2$ and then looping around at $s_2$
    	satisfies this as $a$ is positive.}
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Determine whether ${\cal M}_1, s_1 \models
    \Forever a$ and explain why or why not.
    \begin{answer}
    	${\cal M}_1, s_1 \models \Forever a$ is false. \newline
    	\textit{ ${\cal M}_1, s_1 \models \Forever a$ indicates
    	that any path taken should lead to a state where '$a$' is true! In the
    	path $s_1 - s_3 - (s_4)$ we see '$a$' is true only in $s1$.}
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Find a path from the initial state $s_1$
    which satisfies $a \Until b$.
    \begin{answer}
    	\textbf{Path}: $s_1 - s_3 - (s_4)$	\newline
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Determine whether ${\cal M}_1, s_1 \models
    a \Until b$ and explain why or why not.
    \begin{answer}
    	${\cal M}_1, s_1 \models a \Until b$ is True.	\newline
    	\textbf{Explanation}: At state $s_1$ we have $b$ as true and from
    	definition we don't have to check further once the condition is
    	satisfied. In $s_1$, $a \Until b$ is satisfied hence the statement in the
    	question is true.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Find a path from the initial state $s_1$
    which satisfies $\NextX a \Until \NextX (\neg a \land b)$.
    \begin{answer}
    	\textbf{Path}: $s_1-s_3-(s_4)$	\newline
    	\textit{The path is valid in $s_1$.}
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Determine whether ${\cal M}_1, s_1 \models
    \NextX a \Until \NextX (\neg a \land b)$ and explain why or why not.
    \begin{answer}
    	The statement is False.
    	\newline \textbf{Explanation}: The path $s_1-s_2-(s_4)$ is a path which
    	doesn't satisfy the expression. Given a false path, we cannot say the LHS
    	entails the RHS.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Find a path from the initial state $s_1$
    which satisfies $\NextX \neg b \land \Forever (a \lor \neg b)$.
    \begin{answer}
    	\textbf{Path}: $s_1 - s_2 - (s_4)$.	\newline
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Determine whether ${\cal M}_1, s_1 \models
    \NextX \neg b \land \Forever (a \lor \neg b)$ and explain why or why not.
    \begin{answer}
    	${\cal M}_1, s_1 \models \NextX \neg b \land \Forever (a \lor \neg b)$ is
    	False.	\newline
    	\textbf{Explanation}: In $s_1 - s_3 - (s_4)$, $s_1$ makes $\NextX \neg b$
    	false and in $s_3$ we have $(a \lor \neg b)$ evaluating to false leading
    	to $\Forever (a \lor \neg b)$ being false. The conjunction of a false and
    	any value is always false, hence this expression is false.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Find a path from the initial state $s_1$
    which satisfies $\NextX (\neg a \land b) \land \Sometime (\neg a
    \land \neg b)$.
    \begin{answer}
    	\textbf{Path}: $s_1 - s_3 - (s_4)$	\newline
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Determine whether ${\cal M}_1, s_1 \models
    \NextX (\neg a \land b) \land \Sometime (\neg a \land \neg b)$ and explain why or why not.
    \begin{answer}
    	False since it is not valid for all the paths in the model. \newline
    	\textbf{Explanation}: In the path $s_1 - (s_2)$ we have $\neg a \land b$ as
    	false, therefore $\NextX (\neg a \land b)$ is also false and $\neg a \land
    	\neg b$ is not satisfied anywhere along the path. The conjunction would
    	always be false and hence the expression.
    	 
    	Consider a path $s_1 - (s_2)$. This is one of the path for which the
    	formula is not satisfied in the model. For state $s_2$ $\neg a \land b$  is
    	not true, hence $\NextX (\neg a \land b)$ is also false for state $s_1$. So
    	first part of conjunction of the formula is false. Also to add to this 
    	$\neg a \land \neg b$ is not satified in any of the state in the path. 
    	Hence the formula $\Sometime (\neg a \land \neg b)$ is also false. Thus 
    	for this path and the model the formula is false.
    \end{answer}
    \bigskip
  \end{enumerate}

\item \textbf{[20 points]} List all subformulas of the LTL formula
  \begin{displaymath}
    \NextX \neg p \Until (q \Until ((\Forever r \lor \NextX \Sometime
    \neg q) \implies \NextX r \WeakUntil \neg q))
  \end{displaymath}
  	\begin{answer}
    	The subformulas are as follows:- 
    	\begin{enumerate}
    	  \item $\NextX \neg p \Until (q \Until ((\Forever r \lor \NextX \Sometime
    \neg q) \implies \NextX r \WeakUntil \neg q))$
    	  \item $\NextX \neg p$
    	  \item $\neg p$
    	  \item $p$
    	  \item $q \Until ((\Forever r \lor \NextX \Sometime
    \neg q) \implies \NextX r \WeakUntil \neg q)$
    	  \item $q$
    	  \item $(\Forever r \lor \NextX \Sometime
    \neg q) \implies \NextX r \WeakUntil \neg q$
    	  \item $\Forever r \lor \NextX \Sometime
    \neg q$
    	  \item $\Forever r$
    	  \item $r$
    	  \item	$\NextX \Sometime \neg q$
    	  \item $\Sometime \neg q$
    	  \item $\neg q$
    	  \item $\NextX r \WeakUntil \neg q$
    	  \item $\NextX r$
    	\end{enumerate}
    \end{answer}
    \bigskip
\end{itemize}

\end{document}
