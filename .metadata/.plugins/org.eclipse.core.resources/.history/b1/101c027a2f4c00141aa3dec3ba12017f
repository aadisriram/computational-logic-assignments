\documentclass{article}
\input{503macros.tex}

\title{CSC 503 Homework Assignment 7}
\author{Due October 6, 2014}
\date{September 29, 2014}

\begin{document}
\maketitle

\noindent
In the following expressions, assume that $a,b,c$ are constant
symbols, $f,g,h,i,j,k$ are function symbols, $P,Q$ are predicate symbols,
and $u,v,w,x,y,z$ are variable symbols.

\vspace*{1pc}

\noindent
{[100 points]} Give a resolution refutation of the following set of
clauses.  At each step, indicate the literals being resolved together
and the substitutions being made.  You may find it helpful to
standardize variables apart in each clause and at each step.
  \begin{enumerate}
  \item $\{ \neg R(x_1,y_1), S(y_1) \}$
  \item $\{ \neg R(x_2,y_2), I(x_2,y_2) \}$
  \item $\{ \neg S(y_3), \neg I(x_3,y_3), R(x_3,y_3) \}$
  \item $\{ \neg I(x_4,y_4), I(h(u_4,x_4),m(u_4,y_4)) \}$
  \item $\{ \neg S(y_5),  S(m(x_5,y_5)) \}$
  \item $\{ R(g(x_6),n(g(x_6))) \}$
  \item $\{ \neg R(h(f(x_7),g(x_7)),y_7), R(x_7,y_7) \}$
  \item $\{ \neg R(a,y_8) \}$
  \item $\{ S(b) \}$
  \item $\{ I(b,b) \}$\\
  \item $\{ \neg R(h(f(a), g(a)), y_1) \}$ \hspace{20mm} 8 + 7, $a/x$
  \item $\{ \neg S(y_3), \neg I(h(f(a), g(a)), y_3) \}$ \hspace{8mm} 11 + 3,
  $h(f(a), g(a))/x_3$
  \item $\{ \neg S(m(f(a), y_4)), \neg I(g(a), y_4)\}$  \hspace{7mm} 12 + 4,
  $m(f(a), y4)/y_3), g(a)/x_4$
  \item $\{ \neg S(y_4), \neg I(g(a), y_4)\}$ \hspace{21mm} 13 + 5, $g(a)/x_5$
  \item $\{ \neg R(x_1, y_4), \neg I(g(a), y_4) \}$ \hspace{15.5mm} 14 + 1
  \item $\{ \neg I(g(a), n(g(x_6)))$ \hspace{25.5mm} 15 + 6
  \item $\{ \neg R(g(a), n(g(x_6)))\}$ \hspace{23mm} 16 + 2
  \item $\emptyset$ 17 + 6
  \end{enumerate}

\end{document}
