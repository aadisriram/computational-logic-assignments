\documentclass{article}
\input{503macros.tex}

\usepackage{amsfonts, amsmath, amsthm}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}

\def\Sometime{\mathord{\mathsf{F}}}
\def\Forever{\mathord{\mathsf{G}}}
\def\Next{\mathord{\mathsf{O}}}
\def\NextX{\mathord{\mathsf{X}}}
\def\Until{\mathrel{\mathsf{U}}}
\def\Release{\mathrel{\mathsf{R}}}
\def\WeakUntil{\mathrel{\mathsf{W}}}
\def\Before{\mathrel{\mathsf{B}}}
\def\True{\mathord{\mathsf{true}}}
\def\All{\mathord{\mathsf{A}}}
\def\Exists{\mathord{\mathsf{E}}}
\def\Every{\mathord{\mathsf{E}}}

\title{CSC 503 Homework Assignment 9}
\author{Due October 29, 2014}
\date{October 22, 2014}

\begin{document}
\maketitle

\begin{itemize}
\item \textbf{[40 points total]} Consider the transition model ${\cal
    M}_2$ depicted in Figure \ref{f2} in answering the following
  questions about LTL and CTL statements.
  \begin{figure}[h]
    \centering
    \caption{Model ${\cal M}_2$}
\begin{center}

\begin{tikzpicture}[>=stealth',shorten >=1pt,auto,node distance=3cm]
  \node[state] (q3)      {$s_3: q,r$};
  \node[state] (q4)    [right of=q3] {$s_4: p,q$};
  \node[initial,state] (q1)   [below of=q3]   {$s_1: p,r,t$};
  \node[state] (q2)   [right of=q1]   {$s_2: r$};

  \path[->] 
        (q3) edge         node {} (q4)
        (q4) edge [loop right] node {} (q4)
        (q1) edge         node {} (q3)
        (q2) edge         node {} (q4)
        (q2) edge [loop right] node {} (q2)
        (q1) edge    [bend left] node {} (q2)
        (q2) edge    [bend left] node {} (q1);
\end{tikzpicture}
\end{center}
\label{f2}
\end{figure}
  \begin{enumerate}
  \item \textbf{[8 points]} Does ${\cal M}_2, s_4 \models r \implies
    q$?  Explain why or why not.
    \begin{answer}
    	\textbf{True} \newline
    	\textbf{Explanation}: In $s_4$, $q$ is true hence the implication is
    	always true.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_1 \models r \implies
    q$?  Explain why or why not.
    \begin{answer}
    	\textbf{False} \newline
    	\textbf{Explanation}: In $s_1$, only $r$ is true, we need a true value for
    	$q$ as well for the expression to hold in the model.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_3 \models \Sometime
    \neg t$?  Explain why or why not.
    \begin{answer}
    	\textbf{True} \newline
    	\textbf{Explanation}: We have only $s_3 - s_4$ path from $s_3$, since $t$
    	is false in both states, we get $\neg t$ as true! Hence the expression is
    	true in the model.
    \end{answer}
    \newpage
  \item \textbf{[8 points]} Does ${\cal M}_2, s_1 \models \Sometime
    \neg t$?  Explain why or why not.
    \begin{answer}
    	\textbf{True} \newline
    	\textbf{Explanation}: For all paths starting at $s_1$, the next state has
    	$t$ as false! Hence $\neg t$ will be true in any of the paths from $s_1$
    	and $t$ is true only in $s_1$.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_2 \models \neg \Exists
    \Forever r$?  Explain why or why not.
    \begin{answer}
    	\textbf{False} \newline
    	\textbf{Explanation}: For the path looping in $s_2$, $r$ is always true!
    	Hence the expression is not valid.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_4 \models \neg \Exists
    \Forever r$?  Explain why or why not.
    \begin{answer}
    	\textbf{True}	\newline
    	\textbf{Explanation}: $s_4$ has only a path looping in $s_4$ itself. We
    	have $\neg \Exists \Forever r$ is $\All \Sometime \neg r$. From this we
    	read that the expression says, $\neg r$ is true sometime in the future. IN
    	$s_4$ we have $r$ is false and hence $\neg r$ as true. Hence expression is
    	valid.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_3 \models \Exists
    \Sometime q$?  Explain why or why not.
    \begin{answer}
    	${\cal M}_2, s_3 \models \Exists \Sometime q$ is True.	\newline
    	\textbf{Explanation}:  $s_3-(s_4)$ is the only path from $s_3$ and in this
    	we have $q$ is true in $s_3$ and $s_4$. Thus $\Exists \Sometime q$ is true.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_1 \models \Exists
    \Sometime q$?  Explain why or why not.
    \begin{answer}
    	${\cal M}_2, s_1 \models \Exists \Sometime q$ is True.	\newline	
    	\textbf{Explanation}: The formula $\Exists \Sometime q$ means that there
    	exists atleast one path where $\Sometime q$ is true. Consider a path
    	$s_1-s_3-(s_4)$ which is one of the path in the Model. In this path $q$ is
    	true in state $s_3$ as well as $s_4$. Thus $\Sometime q$ is also true. And
    	since we found one path where $\Sometime q$ is true, thus $\Exists
    	\Sometime q$ is also true.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_4 \models \All
    \NextX \All \Forever (p \lor q)$?  Explain why or why not.
    \begin{answer}
    	${\cal M}_2, s_4 \models \All \NextX \All \Forever (p \lor q)$ is True.
    	\newline
    	\textbf{Explanation}: The formula $\All \NextX \All \Forever (p \lor q)$
    	means that the formula $\All \Forever (p \lor q)$ is true next on all
    	paths. In the model starting from state $s_4$ there is only one path
    	$(s_4)$. On this path $(p \lor q)$ is always true starting from the second
    	node which is again $s_4$. Thus $\All \NextX \All \Forever (p \lor q)$ is
    	true as there is only one path and the formula holds in that path.
    \end{answer}
    \bigskip
  \item \textbf{[8 points]} Does ${\cal M}_2, s_2 \models \All \NextX
    \All \Forever (p \lor q)$?  Explain why or why not.
    \begin{answer}
    	${\cal M}_2, s_2 \models \All \NextX \All \Forever (p \lor q)$ is False.
    	\newline
    	\textbf{Explanation}: The formula $\All \NextX \All \Forever (p \lor q)$
    	means that the formula $\All \Forever (p \lor q)$ is true next on all
    	paths. Consider one of the paths as $(s_2)$, which is looping through state
    	$s_2$. In this path neither $p$ nor $q$ is ever true. Thus we know that for
    	this particular path violates the formula. Hence $\All \NextX \All
    	\Forever (p \lor q)$ is false.
     \end{answer}
    \bigskip
  \end{enumerate}

%\newpage

\item \textbf{[20 points]} List all subformulas of the CTL formula,
  using Convention 3.13 (p.\ 209) to resolve ambiguities.
  \begin{displaymath}
    \Exists [\All \NextX p \Until (\neg \Exists \NextX q \land \All
    [\neg p \Until \All \Forever (p \lor q)])]
  \end{displaymath}
\end{itemize}
	\begin{answer}
	The subformula of the given formula is as follows:-
		\begin{enumerate}
		  \item $\Exists [\All \NextX p \Until (\neg \Exists \NextX q \land \All [\neg
		  p \Until \All \Forever (p \lor q)])]$
		  \item $\All \NextX p$
		  \item $p$
		  \item $\neg \Exists \NextX q \land \All [\neg p \Until \All \Forever (p \lor
		  q)]$
		  \item $\neg \Exists \NextX q$
		  \item $\Exists \NextX q$
		  \item $q$
		  \item $\All [\neg p \Until \All \Forever (p \lor q)]$
		  \item $\neg p$
		  \item $\All \Forever (p \lor q)$
		  \item $p \lor q$
		\end{enumerate}
	\end{answer}
\end{document}
