\documentclass{article}
\input{503macros.tex}

\title{CSC 503 Latex Macros}
\author{Jon Doyle}
\date{August 22, 2012}

\begin{document}
\maketitle

Most of the homework in this course will be typeset in \LaTeX.  Some
of the macros come from standard packages, and some from local
definitions.  As the sources of this file and the homeworks show, all
the supplied packages and macro definitions can be loaded by means of
the \LaTeX command \verb+\input{503macros.tex}+.  The file this loads
then loads others.  The course website supplies a file
\texttt{fitchhr.sty}, which is a version of the file
\texttt{fitch.sty} written by Peter Selinger, modified to include
provide names for inference rules used in the Huth and Ryan textbook.

You are strongly encouraged to typeset your answers for readability,
but you need not typeset your answers in \LaTeX.  All answer
submissions should include a PDF version of the answers.

You are encouraged to prepare your answers by simply adding them into
a copy of the homework source.  Use the constructs
\verb+\begin{answer}+ and \verb+\end{answer}+ to begin and end your
answer.  For example, the code
\begin{verbatim}
\begin{answer}
I think, therefore I am.
\end{answer}
\end{verbatim}
produces the result
\begin{answer}
I think, therefore I am.
\end{answer}


\section{Macros for symbols}

Note that in \LaTeX, the command \verb+\not+ means negation of a
relation, and does not mean logical negation.  Thus \verb+$\neg p$+
produces $\neg p$, while \verb+$p \not\implies q$+ produces $p
\not\implies q$.

\begin{center}
  \begin{tabular}[h]{lll}
    Symbol & LaTeX & Meaning \\ \hline
    $\land$ & \verb+\land+ &  conjunction \\
    $\lor$ & \verb+\lor+ & disjunction \\
    $\neg$ & \verb+\neg+ & negation \\
    $\implies$ & \verb+\implies+ & implication \\
    $\iff$ & \verb+\iff+ & biconditional \\
    $\forall$ & \verb+\forall+ & universal quantfier \\
    $\exists$ & \verb+\exists+ & existential quantifier \\
    $\models$ & \verb+\models+ & entails \\
    $\Cn$ & \verb+\Cn+ & entailed consequences \\
    $\turn$ & \verb+\turn+ & derives (``turnstile'') \\
    $\Th$ & \verb+\Th+ & derived theorems \\
    $\neq$ & \verb+\neq+ & pure inequality \\
    $\defeq$ & \verb+\defeq+ & definitional equality \\
    $x^s$ & \verb+x^s+ & superscript \\
    $x_s$ & \verb+x_s+ & subscript \\
    $\N$ & \verb+\N+ & Natural numbers \\
    $\R$ & \verb+\R+ & Real numbers \\
    $\Z$ & \verb+\Z+ & Integers \\
    $\bot$ & \verb+\bot+ & contradiction, the false \\
    $\top$ & \verb+\top+ & tautology, the true \\
  \end{tabular}
\end{center}


\newpage
\section{Macros for proofs}

Rather than use Huth and Ryan's box-oriented depiction of the
inference rules, we will use a sequential style due to Frederic Fitch.
LaTeX macros for these are available in the fitchhr.sty file provided
on the website, which also provides the original files for the
fitch.sty file.  The file fitchhr.sty differs from fitch.sty only by
changing the appearance of names for rules to correspond to the
notation in the Huth and Ryan textbook, and by adding some rules that
do not appear explicitly in the fitch.sty list.  The additional rule
names are as follows.

\begin{center}
  \begin{tabular}[h]{lll}
    Symbol & LaTeX & Meaning \\ \hline
    {$\wedge$e$_1$} & \verb+\aeone+ & and elimination to conjunct 1 \\
    {$\wedge$e$_2$} & \verb+\aetwo+ & and elimination to conjunct 2 \\
    {$\vee$i$_1$} & \verb+\oione+ & or introduction from disjunct 1 \\
    {$\vee$i$_1$} & \verb+\oitwo+ & or introduction from disjunct 2\\
    copy & \verb+\copy+ & copy or reiteration of a prior statement \\
    MT & \verb+\mt+ & Modus Tollens\\
    PBC & \verb+\pbc+ & Proof By Contradiction \\
    LEM & \verb+\lem+ & Law of the Excluded Middle \\
    premise & \verb+\premise{}+ & premise of a sequent \\
    assumption & \verb+\assumption{}+ & assumption of a hypothetical proof 
  \end{tabular}
\end{center}

The following is a simple example illustrating the usage of this
package.  For detailed instructions and additional functionality, see
the user guide, which can be found in the file fitchdoc.tex.

\[
\begin{nd}
  \hypo{1}  {P\vee Q}   \premise{}
  \hypo{2}  {\neg Q}    \premise{}           
  \open                              
  \hypo{3a} {P}         \assumption{}
  \have{3b} {P}         \copy{3a}
  \close                   
  \open
  \hypo{4a} {Q}        \assumption{}
  \have{4b} {\neg Q}   \copy{2}
  \have{4c} {\bot}     \ne{4a,4b}
  \have{4d} {P}        \be{4c}
  \close                             
  \have{5}  {P}        \oe{1,3a-3b,4a-4d}                 
\end{nd}
\]



\newpage
\section{Sample truth tables}

A connective truth table:
\begin{displaymath}
  \begin{array}[t]{|c|c|c|} \hline
    \alpha & \beta & (\alpha \lor \beta) \\ \hline\hline
    T & T & T \\ \hline
    T & F & T \\ \hline
    F & T & T \\ \hline
    F & F & F \\ \hline
  \end{array}
\end{displaymath}
A complete truth table for $P \leftrightarrow ((P \leftrightarrow P)
\leftrightarrow (\neg P \leftrightarrow \neg P))$:
\begin{displaymath}
  \begin{array}{c|c|c|c|c|c}
    P & \neg P &
    P \leftrightarrow P &
    \neg P \leftrightarrow \neg P &
    (P \leftrightarrow P) \leftrightarrow (\neg P \leftrightarrow \neg P) &
    P \leftrightarrow ((P \leftrightarrow P) \leftrightarrow (\neg P
    \leftrightarrow \neg P))
    \\ \hline
    T & F & T & T & T & T \\
    F & T & T & T & T & F \\
  \end{array}
\end{displaymath}
An abbreviated truth table for $P \leftrightarrow ((P \leftrightarrow
P) \leftrightarrow (\neg P \leftrightarrow \neg P))$:
\begin{displaymath}
  \begin{array}{c|c|c}
    P & \neg P &
    P \leftrightarrow ((P \leftrightarrow P) \leftrightarrow (\neg P
    \leftrightarrow \neg P))
    \\ \hline
    T & F & T \\
    F & T & F \\
  \end{array}
\end{displaymath}


\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
